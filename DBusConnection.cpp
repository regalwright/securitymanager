#include "DBusConnection.h"
#include "SecurityManager.h"

#include <QDBusConnection>
#include <QDBusError>

constexpr const char *SERVICE_NAME = "org.tom.SecurityManager";

DBusConnection::DBusConnection(SecurityManager *parent)
  : QDBusAbstractAdaptor(parent),
  m_parent(parent)
{
    QDBusConnection::sessionBus().registerObject("/", parent);
    if (!QDBusConnection::sessionBus().registerService(SERVICE_NAME)) {
        fprintf(stderr, "%s\n", qPrintable(QDBusConnection::sessionBus().lastError().message()));
        throw std::runtime_error("Failed to find the DBUS session");
    }
}

QDBusVariant DBusConnection::run(const QString &fullPath, const QStringList &arguments)
{
    return QDBusVariant(m_parent->startApplicationRequest(fullPath, arguments));
}
