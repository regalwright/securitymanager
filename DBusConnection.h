#ifndef DBUSCONNECTION_H
#define DBUSCONNECTION_H

#include <QDBusAbstractAdaptor>
#include <QDBusVariant>

class SecurityManager;

class DBusConnection : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.tom.SecurityManager")
public:
    explicit DBusConnection(SecurityManager *parent);

public slots:
    QDBusVariant run(const QString &fullPath, const QStringList &arguments);

private:
    SecurityManager *m_parent;
};

#endif
