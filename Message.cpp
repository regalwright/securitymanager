#include "Message.h"

#include <string.h>
#include <stdexcept>
#include <cassert>

#include <QDebug>

enum FieldType {
    ExecutablePath = 10,
    HomeDir,
    Argument,

    RBindMountSource = 20, // mount from an existing directory.
    RBindMountDest,
    UnMountDir,            // unmount (remove) an existing mountpoint.
};

Message::Message(int size)
    : m_reservedSize(size)
{
    assert(size > 0);
    m_buf = std::shared_ptr<char>(new char[size], std::default_delete<char[]>());
    m_writePtr = m_buf.get();
    memset(m_writePtr, 0, m_reservedSize);
}

Message::Message(char *buffer, int bufferSize)
    : m_reservedSize(bufferSize)
{
    m_buf = std::shared_ptr<char>(new char[bufferSize], std::default_delete<char[]>());
    memcpy(m_buf.get(), buffer, bufferSize);
    buffer = m_buf.get();

    const char *end = buffer + bufferSize;
    while (buffer < end) {
        if (*buffer == ExecutablePath) {
            m_path = buffer + 1;
        }
        else if (*buffer == HomeDir) {
            m_homedir = buffer + 1;
        }
        else if (*buffer == Argument) {
            break;
        }
        const int len = strlen(buffer + 1);
        if (buffer + len + 2 > end) {
            throw std::runtime_error("buffer too small");
        }
        buffer += len + 2;
    }
}

char *Message::path() const
{
    return m_path;
}

void Message::setPath(const std::string &path)
{
    m_path = m_writePtr + 1;
    addString(ExecutablePath, path);
}

char *Message::homedir() const
{
    return m_homedir;
}

void Message::setHomedir(const std::string &path)
{
    m_homedir = m_writePtr + 1;
    addString(HomeDir, path);
}

void Message::addArgument(const char *string)
{
    addString(Argument, string);
}

void Message::printFields() const
{
#ifndef NDEBUG
    qDebug().nospace() << "Message[" << path() << "] " << size() << " bytes";

    const char *buffer  = m_buf.get();
    const char *end = buffer + m_reservedSize;
    while (buffer < end) {
        std::string prefix;
        const char type = *buffer;
        if (type == HomeDir) {
            prefix = "homedir: ";
        }
        else if (type == Argument) {
            prefix = "argument:";
        }
        else if (type == RBindMountSource) {
            prefix = "rbind-src: ";
        }
        else if (type == RBindMountDest) {
            prefix = "rbind-dst: ";
        }
        else if (type == UnMountDir) {
            prefix = "umountDir: ";
        }
        else if (type == 0) {
            qDebug() << Qt::endl;
            return;
        }
        const int len = strlen(buffer + 1);
        if (buffer + len + 2 > end) {
            qWarning() << "buffer overrun detected, str len:" << len;
            return;
        }
        if (type != ExecutablePath) {
            qWarning() << "  " << prefix.c_str() << QString::fromLocal8Bit(buffer + 1, len);
        }
        buffer += len + 2;
    }
#endif
}

char *Message::begin() const
{
    return m_buf.get();
}

int Message::size() const
{
    if (m_writePtr)
        return m_writePtr - m_buf.get();
    return m_reservedSize;
}

void Message::addUmountPoint(const std::string &dir)
{
    addString(UnMountDir, dir);
}

void Message::addRemount(const std::string &source, const std::string &destination)
{
    addString(RBindMountSource, source); // always first
    addString(RBindMountDest, destination); // make sure this is last as it 'executes' the whole thing.
}

void Message::addString(char type, const std::string &string)
{
    assert(m_writePtr);
    if (m_buf.get() + m_reservedSize < m_writePtr + string.size() + 2) {
        // won't fit.
        throw std::runtime_error("String does not fit buffer");
    }
    m_writePtr[0] = type;
    memcpy(++m_writePtr, string.c_str(), string.size());
    m_writePtr += string.size();
    m_writePtr[0] = 0; // trailing zero
    ++m_writePtr;
}


// ///////////////////////////////////////////////////////////////

Message::Iterator::Iterator(const Message * const message)
    : m_parent(message),
      m_cur(message->begin()),
      m_recordSize(-1)
{
}

bool Message::Iterator::isArgument() const
{
    assert(isValid());
    return m_cur[0] == Argument;
}

bool Message::Iterator::isUnmount() const
{
    assert(isValid());
    return m_cur[0] == UnMountDir;
}

bool Message::Iterator::isRemount() const
{
    assert(isValid());
    return m_cur[0] == RBindMountSource;
}

bool Message::Iterator::isValid() const
{
    assert(m_parent);
    assert(m_cur);
    assert(m_cur >= m_parent->begin());
    if (m_cur < m_parent->begin() + m_parent->size()) {
        return m_cur[0] != 0;
    }
    return false;
}

MountMessage Message::Iterator::mountData()
{
    assert(m_parent);
    MountMessage rc;
    if (m_cur[0] == RBindMountSource)
        rc.type = MountMessage::Remount;
    else if (m_cur[0] == UnMountDir)
        rc.type = MountMessage::Umount;
    else
        throw std::runtime_error("Not mount data");

    m_recordSize = 0;
    while (true) {
        char t = m_cur[m_recordSize];
        const char *str = m_cur + m_recordSize + 1;
        const int strSize = strlen(m_cur + m_recordSize + 1);
        m_recordSize += strSize + 2;
        checkAvail(m_recordSize);
        if (t == RBindMountSource) {
            rc.src = std::string(str, strSize);
        }
        else if (t == RBindMountDest) {
            rc.dst = std::string(str, strSize);
            break; // last in the sequence
        }
        else if (rc.type == MountMessage::Umount) {
            rc.src = std::string(str, strSize);
            // only the src field is expected
            break;
        }
    }

    return rc;
}

const char *Message::Iterator::argument()
{
    assert(m_parent);
    assert(isArgument());

    m_recordSize = strlen(m_cur + 1) + 2;
    checkAvail(m_recordSize);
    return m_cur + 1;
}

void Message::Iterator::next()
{
    assert(m_cur);
    if (m_recordSize == -1) { // if not yet calculated, do so now
        // find the size
        switch (*m_cur) {
        case ExecutablePath: // fall through
        case HomeDir: // fall through
        case Argument:
            m_recordSize = strlen(m_cur + 1) + 2;
            break;
        case RBindMountSource: // fall through
        case UnMountDir:
            (void) mountData();
            assert(m_recordSize > 0);
            break;
        case RBindMountDest:
            assert(false);
        default:
            assert(false);
        }
    }
    m_cur = m_cur + m_recordSize;
    m_recordSize = -1;
}

void Message::Iterator::checkAvail(int bytes) const
{
    if (bytes < 0 || bytes > MAX_SIZE)
        throw std::runtime_error("Serialization failure: impossible size");
    if (m_parent->begin() + m_parent->size() < m_cur + bytes)
        throw std::runtime_error("Message::Iterator: out of bounds");
}
