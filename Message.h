#ifndef MESSAGE_H
#define MESSAGE_H

#include <memory>
#include <string>

struct MountMessage {
    enum Type {
        Remount,
        Umount
    };
    Type type;
    std::string src;
    std::string dst;
};

class Message
{
public:
    static const int MAX_SIZE = 4096;
    Message() = delete;
    Message(int size);
    explicit Message(char *buffer, int bufferSize);
    Message(const Message &other) = default;


    /// Sets path to the executable to be run.
    void setPath(const std::string &path);
    char *path() const;

    /// Set 'homedir' (to be used for `export HOME`)
    void setHomedir(const std::string &path);
    char *homedir() const;

    /// Append an argument to the path() executable
    void addArgument(const char *string);

    Message &operator=(const Message &other) = default;

    /// debug method (only has content when NDEBUG is not defined)
    void printFields() const;

    /// raw getter of data
    char *begin() const;
    /// returns size
    int size() const;

    enum AllowExecEnum {
        AllowExec,      ///< The app gets a homedir where (downloaded) files can be executed.
        DisallowExec    ///< Only system-owned apps can be started.
    };

    void addRemount(const std::string &source, const std::string &destination);
    void addUmountPoint(const std::string &dir);

    class Iterator {
    public:
        explicit Iterator(const Message * const message);

        bool isArgument() const;
        bool isUnmount() const;
        bool isRemount() const;
        bool isValid() const;

        MountMessage mountData();
        const char *argument();

        void next();
        void operator++() {
            next();
        }

    private:
        void checkAvail(int bytes) const;

        const Message *m_parent;
        const char *m_cur;
        int m_recordSize;
    };

    Iterator iBegin() const {
        return Iterator(this);
    }

private:
    void addString(char type, const std::string &string);


    char *m_path = nullptr;
    char *m_homedir = nullptr;

    std::shared_ptr<char> m_buf;
    char *m_writePtr = nullptr;
    int m_reservedSize = 0;
    const char *m_firstArgument = nullptr;
};

#endif
