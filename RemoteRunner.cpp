#include "RemoteRunner.h"
#include "Message.h"

#include <QCoreApplication>

#include <unistd.h>

RemoteRunner::RemoteRunner(int inputId, int outputId)
    : m_thread(inputId),
      m_outputId(outputId)
{
    connect (&m_thread, SIGNAL(receivedMessage(QByteArray)),
             this, SIGNAL(receivedMessage(QByteArray)), Qt::QueuedConnection);

    m_thread.start();
}

RemoteRunner::~RemoteRunner()
{
    m_thread.closeConnection();
    m_thread.wait();
}

void RemoteRunner::runRemote(const Message &message)
{
    assert(message.size() > 0);
    assert(message.size() < 0x7FFF);
    char sizeIndicator[2];
    uint32_t messageSize = message.size();
    sizeIndicator[0] = messageSize % 255;
    sizeIndicator[1] = messageSize >> 8;
    write(m_outputId, sizeIndicator, 2);
    write(m_outputId, message.begin(), message.size());
}


// ///////////////////////////////////////////

RemoteRunnerPrivate::RemoteRunnerPrivate(int inputId)
    : m_inputId(inputId)
{
}

void RemoteRunnerPrivate::closeConnection()
{
    close (m_inputId);
}

void RemoteRunnerPrivate::run()
{
    while (true) {
        char buf[4096];
        ssize_t amount = read(m_inputId, buf, 4096);
        if (amount <= 0) {
            // printf("remote runner private got read: %ld, closing down\n", amount);
            QCoreApplication::quit();
            return;
        }
        QByteArray bytes(buf, amount);
        // TODO split messages?
        emit receivedMessage(bytes);
    }
}
