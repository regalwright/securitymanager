#ifndef REMOTERUNNER_H
#define REMOTERUNNER_H

#include <QThread>

class Message;

class RemoteRunnerPrivate : public QThread
{
    Q_OBJECT
public:
    explicit RemoteRunnerPrivate(int inputId);

    void closeConnection();

signals:
    void receivedMessage(QByteArray data);

protected:
    void run();

private:
    int m_inputId;
};


/**
 * Our communication front-end to the runner.
 *
 * In our app we have a DBUs listener in one process and a application-runner in
 * another process. The DBUS listener process uses an RemoteRunner instance to send
 * messages to the other process (the remote) which runs them.
 *
 * Notice that the communication is bi-directional, commands one direction and
 * (error) messages come back.
 *
 * As an implementation detail; we use a blocking read (2) to wait for incoming data,
 * which is done in a private thread. This class uses Qt signals to keep
 * everything thread-safe.
 */
class RemoteRunner : public QObject
{
    Q_OBJECT
public:
    RemoteRunner(int inputId, int outputId);
    ~RemoteRunner();

    void runRemote(const Message &message);

signals:
    void receivedMessage(QByteArray data);

private:
    RemoteRunnerPrivate m_thread;
    int m_outputId;
};


#endif
