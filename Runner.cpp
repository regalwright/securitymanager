#include "Runner.h"

#include <cassert>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mount.h>
#include <signal.h>
#include <unistd.h>

Runner::Runner(const Message &message, int errorFile)
    : m_errorFile(errorFile),
    m_message(message)
{
}

void Runner::setOwnerUserId(uint32_t uid)
{
    assert(uid > 0);
    m_ownerUid = uid;
}

void Runner::setArgs(int argc, char **argv)
{
    m_processName = (argc > 0) ? argv[0] : nullptr;
}

void Runner::run()
{
    // check basic correctness.
    if (m_message.homedir() == nullptr || m_message.homedir()[0] != '/') {
        writeError("Invalid request");
        return;
    }
    const char *homedir = getenv("HOME");

    struct stat homeDirDetails;
    if (lstat(m_message.homedir(), &homeDirDetails)) {
        writeError("Missing homedir arg");
        return;
    }
    if ((homeDirDetails.st_mode & S_IFDIR) != S_IFDIR) { // is not dir
        writeError("Homedir should be a dir");
        return;
    }

    pid_t pid = fork();
    if (pid == -1) {
        fprintf(stderr, "Runner: Failed to fork\n");
        return;
    }
    if (pid) {// we are the parent
        // printf("Fork one done, created %d (I'm %d). going back to listening\n", pid, getpid());
        return;
    }


    /*
     * 'unshare' is the nicest way to run a program in a new namespace.
     * PID 'unshare' means that the to-be-run application can not see which other
     *      processes are running outside of its own, app-local, namespace.
     * IPC 'unshare' means that shared memory, pipes etc which running processes
     *     may make available are likewise namespaced away from the application.
     * NEWNS 'unshare' means we fork the filesystem, allowing us to do mounts
     *     and umounts that change what the child process can see.
     */
    if (-1 == unshare(CLONE_NEWIPC | CLONE_NEWPID | CLONE_NEWNS)) {
        fprintf(stderr, "Runner: Failed to unshare\n");
        exit(0);
    }

    signal(SIGINT, SIG_IGN);
    signal(SIGTERM, SIG_IGN);
    // we make a second fork of the process because that also forks the PID namespace.
    // the child is (likely) getting PID 1
    pid = fork();
    if (pid == -1) {
        fprintf(stderr, "Runner: Failed to fork\n");
        return;
    }

    if (pid) {
        // printf("Fork 2 done, created %d (I'm %d). waiting for child to exit\n", pid, getpid());
        if (m_processName) {
             // rename process
            const auto curLength = strlen(m_processName);
            if (strlen(m_processName) >= 6) {
                memcpy(m_processName, "runner", 7);
                for (int i = 6; i < curLength; ++i) {
                    m_processName[i] = 0;
                }
            }
        }
        // remove SIGCHILD handling we inherited from parent.
        struct sigaction act;
        memset(&act, 0, sizeof(act));
        sigaction(SIGCHLD, &act, 0);

        // This small process waits until the child finished
        // the 'wait' also cleans up the kernel process table and afterwards
        // we simply exit.
        int status;
        waitpid(pid, &status, 0);
        signal(SIGINT, SIG_DFL);
        signal(SIGTERM, SIG_DFL);
        exit(0);
    }

    // mounts, first propagation
    if (mount("none", "/", NULL, MS_REC | MS_PRIVATE, NULL)) {
        fprintf(stderr, "cannot change root filesystem propagation\n");
        exit(1);
    }
    // get us a proc
    if (mount("proc", "/proc", "proc", MS_NOSUID|MS_NOEXEC|MS_NODEV, NULL)) {
        fprintf(stderr, "Runner: Failed to mount proc\n");
        exit(1);
    }
    // handle /tmp
    if (umount2("/tmp", UMOUNT_NOFOLLOW) != -1) {
        if (mount("tmpfs", "/tmp", "tmpfs", MS_NOSUID|MS_NOEXEC|MS_NODEV, NULL)) {
            fprintf(stderr, "Runner: Failed to re-mount /tmp\n");
            exit(1);
        }
        // handle /var/tmp
        umount2("/var/tmp", UMOUNT_NOFOLLOW); // optional
        if (mount("/tmp", "/var/tmp", nullptr, MS_BIND, nullptr)) {
            fprintf(stderr, "Runner: Failed to re-mount /var/tmp\n");
            exit(1);
        }
    }

    int argCount = 0;
    for (auto i = m_message.iBegin(); i.isValid(); i.next()) {
        if (i.isArgument())
            ++argCount;
        else if (i.isUnmount()) {
            auto md = i.mountData();
            if (md.src.empty())
                exit(1); // internal error.
            if (umount2(md.src.c_str(), UMOUNT_NOFOLLOW) == -1) {
                fprintf(stderr, "Runner: umount failed %d: %s\n", errno, md.src.c_str());
                exit(1);
            }
        }
        else if (i.isRemount()) {
            auto md = i.mountData();
            if (md.src.empty() || md.dst.empty())
                exit(1); // internal error.
            // TODO make sure that all mounts are subdirs of 'homedir'
            if (mount(md.src.c_str(), md.dst.c_str(), nullptr, MS_BIND | MS_REC, nullptr)) {
                fprintf(stderr, "Runner: rbind failed %d\n", errno);
                exit(1);
            }
        }
    }
    // Prepare for normal users
    if (chdir(homedir)) {
        fprintf(stderr, "Runner: Failed to change to dir\n");
        exit(1);
    }
    if (setuid(m_ownerUid) != 0 || seteuid(m_ownerUid) != 0) {
        fprintf(stderr, "Runner: Failed to change UID\n");
        exit(1);
    }

    // no point keeping those open.
    fclose(stdin);
    fclose(stdout);
    fclose(stderr);

    if (argCount == 0) {
        char arg[1] = {0};
        execl(m_message.path(), arg, nullptr);
    }
    else {
        char** arguments = new char*[argCount + 2];
        int index = 0;
        arguments[index++] = m_message.path();
        for (auto i = m_message.iBegin(); i.isValid(); i.next()) {
            if (i.isArgument())
                arguments[index++] = const_cast<char*>(i.argument());
        }
        arguments[index] = 0; // always close with a trailing zero
        execv(m_message.path(), arguments);
    }
}

void Runner::writeError(const char *errorMessage)
{
    const int len = strlen(errorMessage);
    write(m_errorFile, errorMessage, len + 1);
}
