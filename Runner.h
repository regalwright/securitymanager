#ifndef RUNNER_H
#define RUNNER_H

#include "Message.h"

class Runner
{
public:
    Runner(const Message &message, int errorFile);

    /**
     * The user Id that owns the security manager.
     */
    void setOwnerUserId(uint32_t uid);
    void setArgs(int argc, char **argv);

    void run();

private:
    void writeError(const char *errorMessage);

    const int m_errorFile;
    uint32_t m_ownerUid = 0;
    Message m_message;

    char *m_processName = nullptr;
};

#endif
