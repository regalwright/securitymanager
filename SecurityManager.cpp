#include "Message.h"
#include "SecurityManager.h"

#include <unistd.h>
#include <sys/stat.h> // for umask

#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <QSettings>
#include <QStringBuilder>
#include <QStandardPaths>

constexpr const char *xauthority = ".Xauthority";


SecurityManager::SecurityManager(int inputId, int outputId)
    : m_runner(inputId, outputId),
      m_listener(this),
      m_dbdir("1/db/")
{
    setObjectName(QLatin1String("SecurityManager")); // For the DBus RPC

    m_basedir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    if (!m_basedir.endsWith("/"))
        m_basedir += "/";
    umask(077); // All we create will only be readable by the owner.

    QDir basedir(m_basedir);
    // Qt bases this on $HOME, so lets make sure we are not being played here.
    if (!basedir.isAbsolute())
        throw std::runtime_error("Config error: datadir has to be absolute");

    bool ok = basedir.mkpath(m_dbdir);
    if (!ok)
        throw std::runtime_error("Failed to write in datadir");
    ok = QDir::setCurrent(m_basedir);
    assert(ok);

    QDirIterator userChecker(".");
    while (userChecker.hasNext()) {
        auto entry = userChecker.next().mid(2); // snip off the "./"
        bool ok;
        int uid = entry.toInt(&ok);
        if (ok && uid >= m_nextAppId)
            m_nextAppId = uid + 1;
    }

    QFileInfo xsec(QDir::home().filePath(xauthority));
    m_hasXSecurity = xsec.exists();
}

QString SecurityManager::startApplicationRequest(const QString &path, const QStringList &arguments)
{
    {
        std::string latin1Path = path.toStdString();
        if (QString::fromStdString(latin1Path) != path)
            return "Path encoding issues encountered";
    }
    if (path.endsWith(".desktop")) {
        /*
         * Supporting simple desktop files is definitely possible here, but as soon as we
         * get to argument parsing, we get to requirements to download remote files before
         * we can feed a local copy to the application, that means we can't possibly supply
         * a full set of support.
         * Which means that desktop file processing needs to be done by the shell / desktop
         * layer before the security manager is even botered with it. So, we simply
         * report an error here.
         */

        return "Desktop files are supposed to be handled by the caller";
    }
    QFileInfo info(path);
    if (!info.exists())
        return "Bad path";
    if (!info.isExecutable())
        return "Not executable";

    /*
     * Lookup path in our database to see if we have an entry for it.
     * Create or update environment.
     */
    auto dbEntry = lookupApp(path);
    assert(dbEntry.appId > 1);

    /*
     * Each app gets its own subdir under m_basedir using its userid as name.
     */
    QDir base(m_basedir);
    QString homedir = QString::number(dbEntry.appId);
    if (!base.mkpath(homedir))
        return QString("Internal error: failed to create environment");
    if (m_hasXSecurity) {
        // The user is running an Xorg session, graphical software won't work
        // unless we copy the .Xauthority file.
        QString target(base.filePath(homedir + '/' + xauthority));
        // don't assume the same session, just copy again.
        QFile::remove(target);
        bool ok = QFile::copy(QDir::home().filePath(xauthority), target);
        if (!ok)
            qWarning() << "Failed copying the xauthority file";
    }

    // then ask the priviledged task to take it from here.
    Message message(Message::MAX_SIZE);
    try {
        message.setPath(path.toStdString());
        message.setHomedir((m_basedir + homedir).toStdString());
        for (auto s : arguments) {
            message.addArgument(s.toUtf8().constData());
        }
    } catch (const std::exception &e) {
        return QString("Limits reached");
    }

    /* Mounts */
    // first unmount everything from the system dir, if its not explicitly approved by the app.
    QSettings system(base.filePath(m_dbdir + "system"), QSettings::IniFormat);
    for (int i = system.beginReadArray("Drives"); i >= 0; --i) {
        system.setArrayIndex(i);
        QString mount = system.value("mount").toString();
        if (!mount.isEmpty() && !dbEntry.m_allowedMounts.contains(mount))
            message.addUmountPoint(mount.toStdString());
    }

    // An application is given its own homedir and we can limit other mounts as well.
    // The first thing to do is to 'overwrite' our application-specific homedir
    // with the user homedir with an 'rbind'.
    message.addRemount(base.absoluteFilePath(homedir).toStdString(), QDir::homePath().toStdString());

#ifndef NDEBUG
    message.printFields();
#endif
    m_runner.runRemote(message);

    return QString("ok");
}

SecurityManager::AppEntry SecurityManager::lookupApp(const QString &path)
{
    QString securePath(path);
    securePath.replace('%', "%25");
    securePath.replace('/', "%2f");
    QFileInfo fi(m_basedir % m_dbdir % "/" % securePath);
    QSettings entry(fi.absoluteFilePath(), QSettings::IniFormat);
    SecurityManager::AppEntry rc;
    if (fi.exists()) {
        rc.appId = entry.value("app-id", 0).toInt();

        for (int i = entry.beginReadArray("AllowedDrives"); i >= 0; --i) {
            entry.setArrayIndex(i);
            rc.m_allowedMounts.append(entry.value("mount").toString());
        }
    }
    else {
        // then assign a fresh one.
        rc.appId = m_nextAppId++;
        entry.setValue("app-id", rc.appId);
    }
    return rc;
}
