#ifndef SECURITYMANAGER_H
#define SECURITYMANAGER_H

#include "RemoteRunner.h"
#include "DBusConnection.h"

#include <QObject>

class SecurityManager : public QObject
{
    Q_OBJECT
public:
    explicit SecurityManager(int inputId, int outputId);

    QString startApplicationRequest(const QString &path, const QStringList &arguments);

private:
    struct AppEntry {
        int appId = 0;

        QStringList m_allowedMounts;
    };

    void readConfig();
    AppEntry lookupApp(const QString &path);

    RemoteRunner m_runner;
    DBusConnection m_listener;

    QString m_basedir;
    QString m_dbdir;
    int m_nextAppId = 0;
    bool m_hasXSecurity = false; // true if the session is Xorg based.
};

#endif
