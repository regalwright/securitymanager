#include "Runner.h"
#include "SecurityManager.h"

#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

#include <QCoreApplication>

// #define DEBUG_MESSAGE

static void sigchld_hdl (int sig)
{
    // Wait for all dead processes.
    // We use a non-blocking call to be sure this signal handler will never block.
    while (waitpid(-1, NULL, WNOHANG) > 0) { }
}


/*
 The runner is the priviledged part that starts processes in their own
 settings.
*/
static void mainRunner(int argc, char **argv, int inputId, int outputId)
{
    const uint32_t ownerUid = getuid(); // the real user, not root.

    /* Create a handler that takes all forked-off processes and gets
       notified when they finish after which we call the sigchld_handler
       method.
       This method will 'wait' on all children, which is required for the
       kernel to cleanup after the children after they finish.
     */
    struct sigaction act;
    memset (&act, 0, sizeof(act));
    act.sa_handler = sigchld_hdl;
    if (sigaction(SIGCHLD, &act, 0) != 0) {
        fprintf(stderr, "Failed to install sigaction handler for SIGCHLD\n");
        exit(1);
    }

    setuid(geteuid()); // become fully root
    /*
     * Loop forever and wait for messages via our pipe, from the mainListener(),
     * then we run those with the settings provided in the message.
     * This keeps the amount of logic that runs under privs to the bare minimum,
     * notice that the Runner will call exec() as the original (non-priviledged) user.
     */
    char buf[Message::MAX_SIZE + 2];
    uint32_t offset = 0;
    while (true) {
        ssize_t amount = read(inputId, buf + offset, sizeof(buf) - offset);
        if (amount == -1) {
            if (errno == EINTR) // try again.
                continue;
            fprintf(stderr, "Failed to read from pipe %d\n", errno);
            exit(0);
        }
        if (amount == 0) // pipe is closed, we should shutdown
            return;
        offset += amount;
        if (offset < 2)
            continue;
        uint32_t messageSize = static_cast<uint8_t>(buf[1]);
        messageSize = messageSize << 8;
        messageSize += static_cast<uint8_t>(buf[0]);
        if (offset < messageSize)
            continue;

        try {
            Message message(buf + 2, offset - 2);
#ifdef DEBUG_MESSAGE
            printf("message [%s] %d bytes\n", message.path(), message.size());
            auto iter = message.iBegin();
            while (iter.isValid()) {
                if (iter.isArgument()) {
                    printf("arg: %s\n", iter.argument());
                }
                if (iter.isRemount()) {
                    auto md = iter.mountData();
                    printf("md: %s -> %s (%s)\n", md.src.c_str(), md.dst.c_str(), md.fsType.c_str());
                }
                if (iter.isUnmount()) {
                    auto md = iter.mountData();
                    printf("umount: %s\n", md.src.c_str());
                }
                iter.next();
            }
#endif
            Runner runner(message, outputId);
            runner.setArgs(argc, argv);
            runner.setOwnerUserId(ownerUid);
            runner.run();
        } catch (const std::exception &e) {
            auto len = strlen(e.what());
            write(outputId, e.what(), len + 1);
        }
        offset = 0;
    }
}

/*
 The listener listens on dbus and does the filesystem stuff and defers
 to the runner to actually start an app
*/
static void mainListener(int x, char **y, int inputId, int outputId)
{
    // drop priviledges
    const int targetUid = getuid(); // the real user, not root.
    if (setuid(targetUid) != 0) { // this is needed for DBUS to connect.
        fprintf(stderr, "Failed to change to user %d\n", targetUid);
        return;
    }
    if (seteuid(targetUid) != 0) {
        fprintf(stderr, "Failed to change to e-user %d\n", targetUid);
        return;
    }

    try {
        QCoreApplication app(x, y);
        app.setApplicationName("secure-run");
        SecurityManager manager(inputId, outputId);
        app.exec();
    } catch (const std::exception &e) {
        fprintf(stderr, "Setup failed with: %s\n", e.what());
    }
}

int main(int x, char **y)
{
    if (geteuid() != 0) {
        fprintf(stderr, "Should be installed suid root\n");
        return 1;
    }
    if (getuid() == 0) {
        fprintf(stderr, "Do not run as root\n");
        return 1;
    }
    if (getgid() == 0 || getegid() == 0) {
        fprintf(stderr, "Warn: group is root, likely setup problem\n");
    }

    int pUp[2]; // from runner to listener
    if (pipe(pUp) == -1) {
        return 1;
    }
    int pDown[2]; // from listener to runner
    if (pipe(pDown) == -1) {
        return 1;
    }

    pid_t pid = fork();
    if (pid < 0) {
        return 1;
    }
    if (pid > 0) {
        close(pUp[1]);
        close(pDown[0]);
        mainListener(x, y, pUp[0], pDown[1]);
        close(pUp[0]);
        close(pDown[1]);
    }
    else {
        close(pUp[0]);
        close(pDown[1]);
        mainRunner(x, y, pDown[0], pUp[1]);
        close(pUp[1]);
        close(pDown[0]);
    }
    return 0;
}
