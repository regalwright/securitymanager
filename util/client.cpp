#include <QtCore/QCoreApplication>
#include <QtDBus/QtDBus>

#include <QDebug>

constexpr const char *SERVICE_NAME = "org.tom.SecurityManager";

class Client: public QObject
{
    Q_OBJECT
public slots:
    void start() {
        struct ShutDown {
            ~ShutDown() { QCoreApplication::quit(); }
        };
        ShutDown shutDownHandler;
        // find our remote
        auto iface = new QDBusInterface(SERVICE_NAME, "/", SERVICE_NAME,
                                        QDBusConnection::sessionBus(), this);
        if (!iface->isValid()) {
            fprintf(stderr, "%s\n",
                    qPrintable(QDBusConnection::sessionBus().lastError().message()));
            return;
        }

        QVariantList list;
        list << QCoreApplication::arguments().at(1);
        QStringList arguments;
        for (auto string : QCoreApplication::arguments().mid(2)) {
            arguments.append(string);
        }
        list.append(arguments);
        auto rc = iface->callWithArgumentList(QDBus::Block, "run", list);
        // For our usage this is enough. I can't seem to find out how
        // to get the contents (the actual string answer) out of the object, so
        // this wil have to do.
        qDebug() << rc;
    }
};


int main(int x, char **y) {

    if (x == 1) {
        fprintf(stderr, "Pass in the application to start\n");
        return 1;
    }
    QCoreApplication app(x, y);

    if (!QDBusConnection::sessionBus().isConnected()) {
        fprintf(stderr, "Cannot connect to the D-Bus session bus.\n");
        return 1;
    }

    Client client;
    QTimer::singleShot(0, &client, &Client::start);
    return app.exec();
}


#include "client.moc"
